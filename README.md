# Selamat Datang di Service Order pada Aplikasi Penjualan

Service ini bertujuan untuk mengelola data-data order yang ada dan menyediakan API sederhana sesuai dengan kebutuhan

Service ini berjalan dengan framework Flask dan dengan ORM SQLAlchemy


Package yang digunakan:

1. Flask
2. json


Fungsi yang ada pada service ini adalah:
1. View all order 
2. View order by oderId
4. Insert new order
5. Update Order
6. Delete order
