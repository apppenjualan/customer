from app import app
from app.controllers import customer_controller
from flask import Blueprint, request,jsonify

customer_blueprint = Blueprint("customer_controller", __name__)

@app.route('/users', methods =["GET"])
def showUsers():
    return customer_controller.shows()

@app.route('/user', methods = ["GET"])
def show():
    param = request.json
    return customer_controller.showUser(**param)

@app.route('/userbyemail', methods = ["GET"])
def showByEmail():
    param = request.json
    return customer_controller.showUserEmail(**param)

@app.route('/user/update', methods = ["POST"])
def update():
    param = request.json
    return customer_controller.updateUser(**param)

@app.route('/user/insert', methods = ["POST"])
def insert():
    param = request.json
    return customer_controller.insertNewUser(**param)

@app.route('/user/delete', methods = ["POST"])
def delete():
    param = request.json
    return customer_controller.deleteUser(**param)

@app.route('/user/requesttoken', methods = ["GET"])
def token():
    param = request.json
    return customer_controller.token(**param)

