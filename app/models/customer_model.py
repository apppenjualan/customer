from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String
from sqlalchemy.sql.functions import user
import re

Base = declarative_base()
engine = create_engine('mysql+mysqlconnector://root:Pythonbackend@localhost:3306/penjualan', echo = True)
Session = sessionmaker(bind = engine)
session = Session()

if session:
    print("Connection Success")

class Customers(Base):
    __tablename__ = 'customer'
    customerId = Column(Integer, primary_key= True)
    namaDepan = Column(String)
    namaBelakang = Column(String)
    email = Column(String)

    def showUsers(self):
        result = session.query(Customers).all()
        return result
    
    def showUserById(self, **params):
        userid = params['customerId']
        result_query = session.query(Customers).filter(Customers.customerId == int(userid))
        return result_query
    
    def showUserByEmail(self, **params):
        email = params['email']
        result_query = session.query(Customers).filter(Customers.email == email)
        return result_query

    def insertUser(self, **params):    
        session.add(Customers(**params))
        session.commit()
    
    def updateUserById(self, **params):
        userId = params['customerId']
        result_query = session.query(Customers).filter(Customers.customerId == int(userId)).one()
        result_query.namaDepan = params['namaDepan']
        session.commit()

        result_query = session.query(Customers).filter(Customers.customerId == int(userId)).one()
        return result_query

    def deleteUserById(self, **params):
        userId = params['customerId']
        result_query = session.query(Customers).filter(Customers.customerId == int(userId)).one()
        session.delete(result_query)
        session.commit()

        result_query = session.query(Customers).all()
        return result_query