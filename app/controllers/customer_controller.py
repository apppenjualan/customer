from app.models.customer_model import Customers
from flask import Flask, jsonify, request
from flask_jwt_extended import *
import json, datetime


mysqldb = Customers()

@jwt_required()
def shows():
    dbresult = mysqldb.showUsers()
    result = []
    for item in dbresult:
        user = {
            "customerId" : item.customerId,
            "namaDepan" : item.namaDepan,
            "namaBelakang" : item.namaBelakang,
            "email" : item.email
        }
        result.append(user)
    return jsonify(result)

def showUser(**params):
    dbresult = mysqldb.showUserById(**params)
    for result in dbresult:
        data = {
            "customerId" : result.customerId,
            "namaDepan" : result.namaDepan,
            "namaBelakang" : result.namaBelakang,
            "email" : result.email
        }
        return jsonify(data)

def showUserEmail(**params):
    dbresult = mysqldb.showUserByEmail(**params)
    for result in dbresult:
        data = {
            "customerId" : result.customerId,
            "namaDepan" : result.namaDepan,
            "namaBelakang" : result.namaBelakang,
            "email" : result.email
        }
        return jsonify(data)

def updateUser(**param):
    mysqldb.updateUserById(**param)
    return jsonify({"message" : "Success"})

def insertNewUser(**param):
    mysqldb.insertUser(**param)
    return jsonify({"message" : "success"})

def deleteUser(**param):
    mysqldb.deleteUserById(**param)
    return jsonify({"message" : "Success"})

def token(**params):
    dbresult = mysqldb.showUserByEmail(**params)
    if dbresult is not None:
        for data in dbresult:
            user = {
                "namaDepan" : data.namaDepan,
                "namaBelakang" : data.namaBelakang,
                "email" : data.email
            }

        expires = datetime.timedelta(days=1)
        access_token = create_access_token(user, fresh=True, expires_delta=expires)

        data = {
            "data" : user,
            "token" : access_token
        }
    else : 
        data = {
            "message" : "email tidak terdaftar"
        }
    
    return jsonify(data)
    